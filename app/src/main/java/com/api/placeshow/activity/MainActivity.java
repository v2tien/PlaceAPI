package com.api.placeshow.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.api.placeshow.R;
import com.api.placeshow.adapter.ViewPagerAdapter;
import com.api.placeshow.common.PrefUtils;
import com.api.placeshow.fragment.FavoriteFragment;
import com.api.placeshow.fragment.PlaceAllFragment;

public class MainActivity extends AppCompatActivity {

    private static final int TAB_PLACE = 0;
    private static final int TAB_FAVORITE = 1;

    private ViewPager mViewPager;
    private TabLayout tabLayout;
    private ViewPagerAdapter mViewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Clear cache
        PrefUtils.clearData();

        initComponent();
        initViewPager();
    }

    private void initComponent(){
        mViewPager = (ViewPager)findViewById(R.id.view_pager);
        tabLayout = (TabLayout)findViewById(R.id.tabs);
    }

    // Create View Pager
    private void initViewPager() {
        mViewPager.setOffscreenPageLimit(2);
        mViewPager.setEnabled(true);
        mViewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        // Add fragment
        mViewPagerAdapter.addFragment(new PlaceAllFragment(), getString(R.string.tab_place));
        mViewPagerAdapter.addFragment(new FavoriteFragment(), getString(R.string.tab_favorite));

        mViewPager.setAdapter(mViewPagerAdapter);
        tabLayout.setupWithViewPager(mViewPager);

        View tabHome = LayoutInflater.from(this).inflate(R.layout.tab_view_pager, null);
        TextView tvTitle = (TextView) tabHome.findViewById(R.id.tv_title);
        tvTitle.setText(getString(R.string.tab_place));
        tvTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
        tabLayout.getTabAt(TAB_PLACE).setCustomView(tabHome);

        View tabCurrent = LayoutInflater.from(this).inflate(R.layout.tab_view_pager, null);
        TextView tvTitle1 = (TextView) tabCurrent.findViewById(R.id.tv_title);
        tvTitle1.setText(getString(R.string.tab_favorite));
        tabLayout.getTabAt(TAB_FAVORITE).setCustomView(tabCurrent);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                TextView tvTitle = (TextView) tab.getCustomView().findViewById(R.id.tv_title);
                // Text color when selected
                tvTitle.setTextColor(getResources().getColor(R.color.colorPrimary));
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // Text color when unselected
                TextView tvTitle = (TextView) tab.getCustomView().findViewById(R.id.tv_title);
                tvTitle.setTextColor(getResources().getColor(R.color.text_normal));
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}
