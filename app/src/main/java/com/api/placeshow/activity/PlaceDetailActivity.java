package com.api.placeshow.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.api.placeshow.R;
import com.api.placeshow.adapter.ReviewAdapter;
import com.api.placeshow.adapter.SliderPagerAdapter;
import com.api.placeshow.common.Constant;
import com.api.placeshow.common.PrefUtils;
import com.api.placeshow.common.Util;
import com.api.placeshow.model.Photo;
import com.api.placeshow.model.Place;
import com.api.placeshow.model.PlaceResponse;
import com.api.placeshow.model.Review;
import com.api.placeshow.retrofit.PlaceClient;
import com.api.placeshow.retrofit.PlaceInterface;
import com.api.placeshow.service.NotifyService;
import com.api.placeshow.view.CusViewPager;
import com.api.placeshow.view.ExpandableHeightListView;
import com.api.placeshow.view.sliderimage.CirclePageIndicator;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PlaceDetailActivity extends AppCompatActivity {

    @BindView(R.id.tv_name_place)
    protected TextView tvNamePlace;
    @BindView(R.id.tv_address_place)
    protected TextView tvAddressPlace;
    @BindView(R.id.tv_phone_place)
    protected TextView tvPhonePlace;
    @BindView(R.id.tv_review)
    protected TextView tvReview;
    @BindView(R.id.view_call)
    protected LinearLayout viewCall;
    @BindView(R.id.view_call_2)
    protected LinearLayout viewCall2;
    @BindView(R.id.view_directions)
    protected LinearLayout viewDirections;
    @BindView(R.id.view_website)
    protected LinearLayout viewWebsite;
    @BindView(R.id.view_location)
    protected LinearLayout viewLocation;
    @BindView(R.id.view_time)
    protected LinearLayout viewTime;
    @BindView(R.id.lv_review)
    protected ExpandableHeightListView lvReview;
    @BindView(R.id.map_view)
    protected MapView mMapView;
    @BindView(R.id.view_pager)
    protected CusViewPager viewPager;
    @BindView(R.id.page_indicator)
    protected CirclePageIndicator circlePageIndicator;
    @BindView(R.id.rating_bar)
    protected RatingBar ratingBar;
    @BindView(R.id.tv_num_rating)
    protected TextView tvNumRating;

    private List<Photo> lstPhoto = new ArrayList<>();
    private List<Review> lstReview = new ArrayList<>();

    private Place mPlace;
    private ReviewAdapter mReviewAdapter;
    private SliderPagerAdapter mPhotoAdapter;

    private String mPlaceId = "";
    private String mNamePlace = "";
    private GoogleMap mGoogleMap;

    private static final int REQUEST_CALL_PHONE = 100;
    private MenuItem btnFavorite;
    private boolean isFavorite = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_detail);
        // init component with butterKnife
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.txt_place_detail);

        mMapView.onCreate(savedInstanceState);

        // Get data intent
        mPlaceId = getIntent().getStringExtra(Constant.PLACE_ID);
        mNamePlace = getIntent().getStringExtra(Constant.NAME);
        // check place is favorite or not favorite
        isFavorite = PrefUtils.getBoolean(mNamePlace, false);
        // Action onClickListener
        initControl();
        // Request place detail
        getPlaceDetail();
    }

    private void initControl() {
        // Intent app google map
        viewDirections.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String uri = "google.navigation:q=" + mPlace.getGeometry().getLocation().getLatPlace() + "," +
                        mPlace.getGeometry().getLocation().getLongPlace();
                Uri gmmIntentUri = Uri.parse(uri);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
            }
        });
        // Action open browse app google map
        viewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse(mPlace.getUrlIntentMap()));
                startActivity(intent);
            }
        });
        // Action open browse
        viewWebsite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse(mPlace.getWebsitePlace()));
                startActivity(intent);
            }
        });
        // Action open call gsm
        viewCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionCall();
            }
        });
        // Action open call gsm
        viewCall2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                actionCall();
            }
        });
    }

    // action open call gsm
    private void actionCall() {
        // check permission action call
        if (ContextCompat.checkSelfPermission(PlaceDetailActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(PlaceDetailActivity.this, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CALL_PHONE);
        } else {
            // intent action call normal
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + mPlace.getPhonePlace()));
            startActivity(callIntent);
        }
    }
    // Get data detail of place
    private void getPlaceDetail() {
        if (Util.checkConnection(this)) {
             //Progress Dialog for User Interaction
            final ProgressDialog dialog;
            dialog = new ProgressDialog(this);
            dialog.setMessage(getString(R.string.txt_loading));
            dialog.show();

            // Call api get place detail
            PlaceInterface api = PlaceClient.getApiService();
            Call<PlaceResponse> call = api.getPlaceDetail(getString(R.string.api_key_place), mPlaceId);
            call.enqueue(new Callback<PlaceResponse>() {
                @Override
                public void onResponse(Call<PlaceResponse> call, Response<PlaceResponse> response) {
                    dialog.dismiss();
                    if (response.isSuccessful()) {
                        // place detail after request api successfully
                        mPlace = response.body().getPlace();
                        // Check status of response api, if status is ok, view data place
                        if (response.body().getStatus().equals(getString(R.string.txt_ok))) {
                            viewTime.setVisibility(View.GONE);
                            viewMap(mPlace.getGeometry().getLocation().getLatPlace(), mPlace.getGeometry().getLocation().getLongPlace());
                            tvNamePlace.setText(mPlace.getNamePlace());
                            // Check null phone of place
                            if (mPlace.getPhonePlace() == null || mPlace.getPhonePlace().isEmpty()) {
                                viewCall2.setVisibility(View.GONE);
                                viewCall.setEnabled(false);
                            } else {
                                viewCall.setEnabled(true);
                                viewCall2.setVisibility(View.VISIBLE);
                                tvPhonePlace.setText(mPlace.getPhonePlace());
                            }
                            // Check null address of place
                            if (mPlace.getAddressPlace() == null || mPlace.getAddressPlace().isEmpty()) {
                                viewLocation.setVisibility(View.GONE);
                            } else {
                                viewLocation.setVisibility(View.VISIBLE);
                                tvAddressPlace.setText(mPlace.getAddressPlace());
                            }
                            // Check null website of place
                            if (mPlace.getWebsitePlace() == null || mPlace.getWebsitePlace().isEmpty()) {
                                viewWebsite.setEnabled(false);
                            } else {
                                viewWebsite.setEnabled(true);
                            }
                            // set rating bar
                            tvNumRating.setText(String.valueOf(mPlace.getRatingPlace()));
                            Double num = mPlace.getRatingPlace();
                            Long rating = Math.round(num * 10);
                            int progress = Integer.valueOf(rating.intValue());
                            ratingBar.setProgress(progress);
                            // get list photo
                            if (mPlace.getPhotoPlace() != null) {
                                for (int i = 0; i < mPlace.getPhotoPlace().size(); i++) {
                                    if (i < 3) {
                                        lstPhoto.add(mPlace.getPhotoPlace().get(i));
                                    }
                                }
                            }
                            // check list photo empty
                            if (!lstPhoto.isEmpty()) {
                                mPhotoAdapter = new SliderPagerAdapter(PlaceDetailActivity.this, lstPhoto);
                                viewPager.setAdapter(mPhotoAdapter);
                                circlePageIndicator.setViewPager(viewPager);
                                viewPager.setCurrentItem(0);
                            } else {
                                viewPager.setVisibility(View.GONE);
                                circlePageIndicator.setVisibility(View.GONE);
                            }

                            // get list review
                            lstReview = mPlace.getReviewPlace();
                            if (lstReview != null) {
                                tvReview.setVisibility(View.VISIBLE);
                                lvReview.setVisibility(View.VISIBLE);
                                if (mReviewAdapter == null) {
                                    mReviewAdapter = new ReviewAdapter(PlaceDetailActivity.this, lstReview);
                                    lvReview.setAdapter(mReviewAdapter);
                                } else {
                                    mReviewAdapter.setListReview(lstReview);
                                }
                                lvReview.setExpanded(true);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<PlaceResponse> call, Throwable t) {
                    // Request api fail
                    dialog.dismiss();
                    Toast.makeText(PlaceDetailActivity.this, t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            // Notification when no connect internet
            Toast.makeText(this, R.string.txt_no_internet, Toast.LENGTH_SHORT).show();
        }
    }

    private void viewMap(final double latitude, final double longitude) {
        mMapView.onResume();
        // show map view
        try {
            MapsInitializer.initialize(this.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;
                // Hide button zom on map view
                mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
                // Disable scroll gestures on map view
                mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
                // Set location for place on map
                LatLng sydney = new LatLng(latitude, longitude);
                mGoogleMap.addMarker(new MarkerOptions().position(sydney));
                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(13).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                // Action click marker on map view
                mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent intent = new Intent(PlaceDetailActivity.this, PlaceOnMapActivity.class);
                        intent.putExtra(Constant.PLACE, mPlace);
                        startActivity(intent);
                    }
                });
                // Action click map view
                mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        Intent intent = new Intent(PlaceDetailActivity.this, PlaceOnMapActivity.class);
                        intent.putExtra(Constant.PLACE, mPlace);
                        startActivity(intent);
                    }
                });
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CALL_PHONE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + mPlace.getPhonePlace()));
                startActivity(callIntent);
            } else {
                Toast.makeText(this, R.string.txt_permission_denied, Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_favorite, menu);
        btnFavorite = menu.findItem(R.id.action_favorite);
        //Show icon when favorite or not favorite
        if (isFavorite == true) {
            btnFavorite.setIcon(R.drawable.ic_star_yellow);
        } else {
            btnFavorite.setIcon(R.drawable.ic_star_white);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // Click back
                onBackPressed();
                break;
            case R.id.action_favorite:
                if (isFavorite == false) {
                    // Click favorite place
                    btnFavorite.setIcon(R.drawable.ic_star_yellow);
                    PrefUtils.putBoolean(mNamePlace, true);
                    NotifyService.notifyFavorite(mPlace);
                } else {
                    // Click unfavorite place
                    btnFavorite.setIcon(R.drawable.ic_star_white);
                    PrefUtils.putBoolean(mNamePlace, false);
                    NotifyService.notifyUnFavorite(mPlace);
                }
                break;
        }
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }
}
