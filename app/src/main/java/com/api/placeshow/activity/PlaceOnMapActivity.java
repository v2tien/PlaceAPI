package com.api.placeshow.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.api.placeshow.R;
import com.api.placeshow.common.Constant;
import com.api.placeshow.common.Util;
import com.api.placeshow.model.Place;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaceOnMapActivity extends AppCompatActivity {

    @BindView(R.id.map_view_2)
    protected MapView mMap;
    @BindView(R.id.tv_name_place_on_map)
    protected TextView tvNamePlace2;
    @BindView(R.id.tv_address_place_on_map)
    protected TextView tvAddressPlace2;
    @BindView(R.id.tv_open_place_on_map)
    protected TextView tvOpenPlace;
    @BindView(R.id.tv_rating_on_map)
    protected TextView tvRatingPlace;
    @BindView(R.id.rating_bar_on_map)
    protected RatingBar ratingBar2;
    @BindView(R.id.imv_place_on_map)
    protected ImageView imvPlace2;
    @BindView(R.id.view_data_place)
    protected LinearLayout viewDataPlace;

    private GoogleMap mGoogleMap;
    private Place mPlace = new Place();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_on_map);
        // init component with butterKnife
        ButterKnife.bind(this);
        mPlace = getIntent().getParcelableExtra(Constant.PLACE);

        mMap.onCreate(savedInstanceState);
        setData();
        // Show location on map
        viewMap(mPlace.getGeometry().getLocation().getLatPlace(), mPlace.getGeometry().getLocation().getLongPlace());
    }
    // Set data for place
    private void setData() {
        if (mPlace.getPhotoPlace() != null) {
            String url = Util.getUrlPhoto(mPlace.getPhotoPlace().get(0).getReferencePhoto());
            Glide.with(PlaceOnMapActivity.this)
                    .load(url)
                    .into(imvPlace2);
        }else{
            imvPlace2.setImageResource(R.drawable.default_photo);
        }
        tvNamePlace2.setText(mPlace.getNamePlace());
        tvAddressPlace2.setText(mPlace.getAddressPlace());
        tvRatingPlace.setText(String.valueOf(mPlace.getRatingPlace()));
        // set rating bar
        Double num = mPlace.getRatingPlace();
        Long rating = Math.round(num * 10);
        int progress = Integer.valueOf(rating.intValue());
        ratingBar2.setProgress(progress);

        // Action back activity
        viewDataPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void viewMap(final double latitude, final double longitude) {
        mMap.onResume();
        // show map
        try {
            MapsInitializer.initialize(this.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMap.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                mGoogleMap = mMap;
                LatLng sydney = new LatLng(latitude, longitude);
                // googleMap.setOnMyLocationChangeListener(myLocationChangeListener);
                mGoogleMap.addMarker(new MarkerOptions().position(sydney));
                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(sydney).zoom(15).build();
                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        mMap.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMap.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMap.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMap.onLowMemory();
    }
}
