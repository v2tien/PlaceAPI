package com.api.placeshow;

import android.app.Application;
import android.content.Context;

import com.api.placeshow.common.Constant;


public class CusApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Constant.sContext = getApplicationContext();
        Constant.sPreferences = getSharedPreferences(Constant.PREF_APP, Context.MODE_PRIVATE);
    }
}
