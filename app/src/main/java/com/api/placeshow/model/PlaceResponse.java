package com.api.placeshow.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PlaceResponse implements Parcelable{
    @SerializedName("next_page_token")
    private String pageToken;
    @SerializedName("results")
    private List<Place> listResult;
    @SerializedName("status")
    private String status;
    @SerializedName("result")
    private Place place;
    @SerializedName("error_message")
    private String errorMessage;

    protected PlaceResponse(Parcel in) {
        pageToken = in.readString();
        listResult = in.createTypedArrayList(Place.CREATOR);
        status = in.readString();
        place = in.readParcelable(Place.class.getClassLoader());
        errorMessage = in.readString();
    }

    public static final Creator<PlaceResponse> CREATOR = new Creator<PlaceResponse>() {
        @Override
        public PlaceResponse createFromParcel(Parcel in) {
            return new PlaceResponse(in);
        }

        @Override
        public PlaceResponse[] newArray(int size) {
            return new PlaceResponse[size];
        }
    };

    public String getPageToken() {
        return pageToken;
    }

    public List<Place> getListResult() {
        return listResult;
    }

    public String getStatus() {
        return status;
    }

    public Place getPlace() {
        return place;
    }

    public void setPlace(Place place) {
        this.place = place;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(pageToken);
        dest.writeTypedList(listResult);
        dest.writeString(status);
        dest.writeParcelable(place, flags);
        dest.writeString(errorMessage);
    }
}
