package com.api.placeshow.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Review implements Parcelable{
    @SerializedName("author_name")
    private String nameReview;
    @SerializedName("text")
    private String contentReview;
    @SerializedName("relative_time_description")
    private String lastTimeReview;
    @SerializedName("time")
    private long timeReview;
    @SerializedName("rating")
    private double ratingOfReview;

    protected Review(Parcel in) {
        nameReview = in.readString();
        contentReview = in.readString();
        lastTimeReview = in.readString();
        timeReview = in.readLong();
        ratingOfReview = in.readDouble();
    }

    public static final Creator<Review> CREATOR = new Creator<Review>() {
        @Override
        public Review createFromParcel(Parcel in) {
            return new Review(in);
        }

        @Override
        public Review[] newArray(int size) {
            return new Review[size];
        }
    };

    public String getNameReview() {
        return nameReview;
    }

    public String getContentReview() {
        return contentReview;
    }

    public String getLastTimeReview() {
        return lastTimeReview;
    }

    public double getRatingOfReview() {
        return ratingOfReview;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(nameReview);
        dest.writeString(contentReview);
        dest.writeString(lastTimeReview);
        dest.writeLong(timeReview);
        dest.writeDouble(ratingOfReview);
    }
}
