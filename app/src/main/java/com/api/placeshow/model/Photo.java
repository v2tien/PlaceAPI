package com.api.placeshow.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Photo implements Parcelable{
    @SerializedName("height")
    private int heightPhoto;
    @SerializedName("photo_reference")
    private String referencePhoto;
    @SerializedName("width")
    private int widthPhoto;

    protected Photo(Parcel in) {
        referencePhoto = in.readString();
        widthPhoto = in.readInt();
        heightPhoto = in.readInt();
    }

    public static final Creator<Photo> CREATOR = new Creator<Photo>() {
        @Override
        public Photo createFromParcel(Parcel in) {
            return new Photo(in);
        }

        @Override
        public Photo[] newArray(int size) {
            return new Photo[size];
        }
    };

    public String getReferencePhoto() {
        return referencePhoto;
    }

    public int getWidthPhoto() {
        return widthPhoto;
    }

    public int getHeightPhoto() {
        return heightPhoto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(referencePhoto);
        dest.writeInt(widthPhoto);
        dest.writeInt(heightPhoto);
    }
}
