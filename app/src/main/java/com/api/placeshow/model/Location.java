package com.api.placeshow.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

// Get location of place
public class Location implements Parcelable{

    @SerializedName("lat")
    private double latPlace;
    @SerializedName("lng")
    private double longPlace;

    protected Location(Parcel in) {
        latPlace = in.readDouble();
        longPlace = in.readDouble();
    }

    public static final Creator<Location> CREATOR = new Creator<Location>() {
        @Override
        public Location createFromParcel(Parcel in) {
            return new Location(in);
        }

        @Override
        public Location[] newArray(int size) {
            return new Location[size];
        }
    };

    public double getLatPlace() {
        return latPlace;
    }

    public double getLongPlace() {
        return longPlace;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeDouble(latPlace);
        dest.writeDouble(longPlace);
    }
}
