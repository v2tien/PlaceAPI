package com.api.placeshow.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Place implements Parcelable{
    @SerializedName("place_id")
    private String idPlace;
    @SerializedName("name")
    private String namePlace;
    @SerializedName("vicinity")
    private String addressPlace;
    @SerializedName("geometry")
    private Geometry geometry;
    @SerializedName("rating")
    private double ratingPlace;
    @SerializedName("international_phone_number")
    private String phonePlace;
    @SerializedName("website")
    private String websitePlace;
    @SerializedName("reference")
    private String referencePlace;
    @SerializedName("url")
    private String urlIntentMap;
    @SerializedName("photos")
    private List<Photo> photoPlace;
    @SerializedName("reviews")
    private List<Review> reviewPlace;


    public Place() {
    }

    protected Place(Parcel in) {
        idPlace = in.readString();
        namePlace = in.readString();
        addressPlace = in.readString();
        geometry = in.readParcelable(Geometry.class.getClassLoader());
        ratingPlace = in.readDouble();
        phonePlace = in.readString();
        websitePlace = in.readString();
        referencePlace = in.readString();
        urlIntentMap = in.readString();
        photoPlace = in.createTypedArrayList(Photo.CREATOR);
        reviewPlace = in.createTypedArrayList(Review.CREATOR);
    }

    public static final Creator<Place> CREATOR = new Creator<Place>() {
        @Override
        public Place createFromParcel(Parcel in) {
            return new Place(in);
        }

        @Override
        public Place[] newArray(int size) {
            return new Place[size];
        }
    };

    public Geometry getGeometry() {
        return geometry;
    }

    public String getUrlIntentMap() {
        return urlIntentMap;
    }

    public String getIdPlace() {
        return idPlace;
    }

    public String getNamePlace() {
        return namePlace;
    }

    public String getAddressPlace() {
        return addressPlace;
    }

    public double getRatingPlace() {
        return ratingPlace;
    }

    public String getPhonePlace() {
        return phonePlace;
    }

    public String getWebsitePlace() {
        return websitePlace;
    }

    public List<Photo> getPhotoPlace() {
        return photoPlace;
    }

    public List<Review> getReviewPlace() {
        return reviewPlace;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(idPlace);
        dest.writeString(namePlace);
        dest.writeString(addressPlace);
        dest.writeParcelable(geometry, flags);
        dest.writeDouble(ratingPlace);
        dest.writeString(phonePlace);
        dest.writeString(websitePlace);
        dest.writeString(referencePlace);
        dest.writeString(urlIntentMap);
        dest.writeTypedList(photoPlace);
        dest.writeTypedList(reviewPlace);
    }
}
