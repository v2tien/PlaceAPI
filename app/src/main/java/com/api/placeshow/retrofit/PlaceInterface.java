package com.api.placeshow.retrofit;

import com.api.placeshow.common.Constant;
import com.api.placeshow.model.PlaceResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PlaceInterface {

    /**
     * getListPlace request get list place
     * @param apiPlace  api key
     * @param location  current location with lat and lng
     * @param radius  radius around
     * @param pageToken  token for next page
     */
    @GET(Constant.GET_PLACE)
    Call<PlaceResponse> getListPlace(@Query(Constant.KEY) String apiPlace, @Query(Constant.LOCATION) String location,
                                     @Query(Constant.RADIUS) double radius, @Query(Constant.SENSOR) boolean isSensor,
                                     @Query(Constant.PAGE_TOKEN_NEXT) String pageToken);

    /**
     * getPlaceDetail request get place detail
     * @param apiPlace  api key
     * @param placeId  id of place
     */
    @GET(Constant.GET_PLACE_DETAIL)
    Call<PlaceResponse> getPlaceDetail(@Query(Constant.KEY) String apiPlace, @Query(Constant.PLACEID) String placeId);
}
