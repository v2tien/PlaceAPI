package com.api.placeshow.retrofit;

import com.api.placeshow.common.Constant;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlaceClient {

    private static Retrofit sRetrofit = null;

    // Initialize retrofit
    public static Retrofit getClient() {
        Gson gson = new GsonBuilder().setLenient().create();

        if (sRetrofit == null) {
            sRetrofit = new Retrofit.Builder().baseUrl(Constant.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create(gson)).build();
        }
        return sRetrofit;
    }

    public static PlaceInterface getApiService() {
        return getClient().create(PlaceInterface.class);
    }
}
