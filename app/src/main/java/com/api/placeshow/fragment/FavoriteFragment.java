package com.api.placeshow.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.api.placeshow.R;
import com.api.placeshow.adapter.FavoriteAdapter;
import com.api.placeshow.common.Constant;
import com.api.placeshow.model.Place;
import com.api.placeshow.service.Notify;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteFragment extends Fragment {
    @BindView(R.id.recycle_view) protected RecyclerView mRecyclerView;
    @BindView(R.id.tv_result) protected TextView tvResult;

    private FavoriteAdapter mFavoriteAdapter;
    private List<Place> favoriteList = new ArrayList<>();
    private StaggeredGridLayoutManager layoutManager;

    private BroadcastReceiver favorite;
    private BroadcastReceiver unFavorite;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorite, container, false);
        // init component with butterKnife
        ButterKnife.bind(this, view);
        initControl();
        registerReceiver();
        return view;
    }

    private void initControl() {
        layoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(layoutManager);

        // Check list favorite empty
        if (favoriteList.isEmpty()) {
            tvResult.setVisibility(View.VISIBLE);
        } else {
            // Show data for recycle view
            tvResult.setVisibility(View.GONE);
            mFavoriteAdapter = new FavoriteAdapter(getActivity(), favoriteList);
            mRecyclerView.setAdapter(mFavoriteAdapter);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }

    public void registerReceiver() {
        // Register receiver info action favorite place
        IntentFilter filter = new IntentFilter(Notify.FAVORITE_PLACE.getValue());
        favorite = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Place place = intent.getParcelableExtra(Constant.PLACE);
                favoriteList.add(place);
                if(mFavoriteAdapter == null){
                    mFavoriteAdapter = new FavoriteAdapter(getActivity(), favoriteList);
                    mRecyclerView.setAdapter(mFavoriteAdapter);
                }else {
                    mFavoriteAdapter.setListFavorite(favoriteList);
                }
                tvResult.setVisibility(View.GONE);
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(favorite, filter);

        // Register receiver info action unfavorite place
        IntentFilter filter1 = new IntentFilter(Notify.UNFAVORITE_PLACE.getValue());
        unFavorite = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Place place = intent.getParcelableExtra(Constant.PLACE);
                for (int i = 0; i < favoriteList.size(); i++) {
                    if (favoriteList.get(i).getNamePlace().equalsIgnoreCase(place.getNamePlace())) {
                        favoriteList.remove(favoriteList.get(i));
                        mFavoriteAdapter.setListFavorite(favoriteList);
                        if (favoriteList.isEmpty()) {
                            tvResult.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(unFavorite, filter1);
    }

    // Unregister receiver
    public void unregisterReceiver() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(favorite);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(unFavorite);
    }
}
