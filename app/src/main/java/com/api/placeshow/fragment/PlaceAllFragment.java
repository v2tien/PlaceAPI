package com.api.placeshow.fragment;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.api.placeshow.R;
import com.api.placeshow.adapter.PlaceAllAdapter;
import com.api.placeshow.common.Constant;
import com.api.placeshow.common.GPSTracker;
import com.api.placeshow.common.Util;
import com.api.placeshow.model.Place;
import com.api.placeshow.model.PlaceResponse;
import com.api.placeshow.retrofit.PlaceClient;
import com.api.placeshow.retrofit.PlaceInterface;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.api.placeshow.common.GPSTracker.checkGPSEnable;

public class PlaceAllFragment extends Fragment {

    @BindView(R.id.recycle_view_all)
    protected RecyclerView recyclerView;
    @BindView(R.id.v_loadmore)
    protected LinearLayout viewLoadMore;
    @BindView(R.id.tv_error_msg)
    protected TextView tvErrorMsg;
    private StaggeredGridLayoutManager layoutManager;

    private static final int REQUEST_LOCATION = 154;
    private static final int KEY_SETTING_GPS = 155;

    private List<Place> lstPlace = new ArrayList<>();
    private PlaceAllAdapter mPlaceAdapter;

    private double mLongitude;
    private double mLatitude;
    private GPSTracker gpsTracker;

    private String mPageToken = "";
    private String mErrorMsg = "";
    private int visibleItemCount;
    private int totalItemCount;
    private int pastVisibleItems;
    private int[] firstVisibleItems = null;
    private boolean isLoadMore = false;
    private Handler handler = new Handler();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place_all, container, false);
        // init component with butterKnife
        ButterKnife.bind(this, view);

        layoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        initControl();

        if (!GPSTracker.checkGPSEnable(getActivity())) {
            showSettingsAlert();
            return view;
        }
        /**
         * check permission device location
         */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                return view;
            }
        }
        getData();

        return view;
    }

    /**
     * get data all place around my location
     */
    private void getListPlace(String location) {
        if (Util.checkConnection(getActivity())) {
            // Progress Dialog for User Interaction
            final ProgressDialog dialog;
            dialog = new ProgressDialog(getActivity());
            dialog.setMessage(getString(R.string.txt_loading));
            if (mPageToken == null || mPageToken.isEmpty()) {
                dialog.show();
            }
            // Call api get list all place
            PlaceInterface api = PlaceClient.getApiService();
            Call<PlaceResponse> call = api.getListPlace(getString(R.string.api_key_place), location, Constant.PLACE_RADIUS, false, mPageToken);
            call.enqueue(new Callback<PlaceResponse>() {
                @Override
                public void onResponse(Call<PlaceResponse> call, Response<PlaceResponse> response) {
                    dialog.dismiss();
                    if (response.isSuccessful()) {
                        mPageToken = response.body().getPageToken();
                        // list place after request api successfully
                        List<Place> places = response.body().getListResult();
                        viewLoadMore.setVisibility(View.GONE);
                        // Check status of response api, if status is ok, view data place
                        if (response.body().getStatus().equals(getString(R.string.txt_ok))) {
                            tvErrorMsg.setVisibility(View.GONE);
                            recyclerView.setVisibility(View.VISIBLE);
                            lstPlace.addAll(places);
                            // Check null adapter
                            if (mPlaceAdapter == null) {
                                mPlaceAdapter = new PlaceAllAdapter(getActivity(), lstPlace);
                                recyclerView.setAdapter(mPlaceAdapter);
                            } else {
                                isLoadMore = false;
                                mPlaceAdapter.setListPlace(lstPlace);
                            }
                        } else {
                            // check list place empty
                            mErrorMsg = response.body().getErrorMessage();
                            if (!lstPlace.isEmpty()) {
                                tvErrorMsg.setVisibility(View.GONE);
                                Toast.makeText(getActivity(), mErrorMsg, Toast.LENGTH_SHORT).show();
                            } else {
                                recyclerView.setVisibility(View.GONE);
                                tvErrorMsg.setVisibility(View.VISIBLE);
                                tvErrorMsg.setText(mErrorMsg);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(Call<PlaceResponse> call, Throwable t) {
                    dialog.dismiss();
                    // Request api fail
                    Toast.makeText(getActivity(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            // Notification when no connect internet
            Toast.makeText(getActivity(), R.string.txt_no_internet, Toast.LENGTH_SHORT).show();
        }
    }

    private void getData() {
        GPSTracker gpsTracker = new GPSTracker(getActivity());
        // get current location
        if (gpsTracker.canGetLocation()) {
//            gpsTracker.getLocation();
            mLongitude = gpsTracker.getLongitude();
            mLatitude = gpsTracker.getLatitude();
            String location = mLatitude + "," + mLongitude;
            getListPlace(location);
        }
    }

    // listener scroll recycle view
    private void initControl() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                visibleItemCount = layoutManager.getChildCount();
                totalItemCount = layoutManager.getItemCount();
                firstVisibleItems = layoutManager.findFirstVisibleItemPositions(firstVisibleItems);
                if (firstVisibleItems != null && firstVisibleItems.length > 0) {
                    pastVisibleItems = firstVisibleItems[0];
                }

                // Check condition next page
                if (pastVisibleItems > 0 && (pastVisibleItems + visibleItemCount) >= totalItemCount) {
                    if (!isLoadMore && mPageToken != null) {
                        isLoadMore = true;
                        viewLoadMore.setVisibility(View.VISIBLE);
                        handler.postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                getListPlace(mPageToken);
                            }
                        }, 1000);
                    }
                }
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Check Permissions Now
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            getData();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == KEY_SETTING_GPS) {
            if (checkGPSEnable(getActivity())) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                            ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(new String[]{android.Manifest.permission.ACCESS_COARSE_LOCATION, android.Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
                        return;
                    }
                }
                getData();
            }
        }
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */
    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        // Setting Dialog Title
        alertDialog.setTitle(R.string.txt_gps_title);
        // Setting Dialog Message
        alertDialog.setMessage(R.string.txt_gps_message);
        // On pressing Settings button
        alertDialog.setPositiveButton(R.string.txt_setting, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivityForResult(intent, KEY_SETTING_GPS);
            }
        });
        // on pressing cancel button
        alertDialog.setNegativeButton(R.string.txt_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                getActivity().finish();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
}
