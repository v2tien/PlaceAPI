package com.api.placeshow.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CusViewPager extends ViewPager {

	private boolean isEnabled = true;

	public CusViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (this.isEnabled) {
			return super.onTouchEvent(event);
		}
		return false;
	}

	public void setEnableSwitchTab(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}

	@Override
	public boolean onInterceptTouchEvent(MotionEvent event) {
		return isEnabled ? super.onInterceptTouchEvent(event) : false;
	}
}
