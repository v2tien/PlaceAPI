package com.api.placeshow.service;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.api.placeshow.common.Constant;
import com.api.placeshow.model.Place;

public class NotifyService {

    // Notify service when click favorite
    public static void notifyFavorite(Place place) {
        Intent intent = new Intent(Notify.FAVORITE_PLACE.getValue());
        intent.putExtra(Constant.PLACE, place);
        LocalBroadcastManager.getInstance(Constant.sContext).sendBroadcast(intent);
    }
    // Notify service when click unfavorite
    public static void notifyUnFavorite(Place place) {
        Intent intent = new Intent(Notify.UNFAVORITE_PLACE.getValue());
        intent.putExtra(Constant.PLACE, place);
        LocalBroadcastManager.getInstance(Constant.sContext).sendBroadcast(intent);
    }
}
