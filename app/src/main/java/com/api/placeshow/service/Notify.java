package com.api.placeshow.service;

public enum Notify {

    FAVORITE_PLACE("com.api.placeshow.service.notify.favorite"),
    UNFAVORITE_PLACE("com.api.placeshow.service.notify.unfavorite");

    private String value;

    private Notify(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
