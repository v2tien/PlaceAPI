package com.api.placeshow.common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.annotation.NonNull;

import com.api.placeshow.R;

public class Util {

    // check network
    public static boolean checkConnection(@NonNull Context context) {
        return ((ConnectivityManager) context.getSystemService
                (Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }
    // get link photo of place
    public static String getUrlPhoto(String photoReference) {
        String url = Constant.GET_PHOTO_PLACE + "?" + Constant.KEY + "=" + Constant.sContext.getString(R.string.api_key_place) + "&" + Constant.SENSOR + "&" +
                Constant.PHOTOREFENCE + "=" + photoReference + "&" + Constant.MAX_WIDTH + "=" + Constant.SIZE_MAX + "&" + Constant.MAX_HEIGHT + "=" + Constant.SIZE_MAX;
        return url;
    }
}
