package com.api.placeshow.common;

import android.content.Context;
import android.content.SharedPreferences;

public class Constant {
    public static Context sContext;
    public static SharedPreferences sPreferences;
    public static final String PREF_APP = "preference";

    public static final double PLACE_RADIUS = 3000; // met
    public static final int SIZE_MAX = 500;
    public static final String PLACE_ID = "place_id";
    public static final String PLACE = "place";
    public static final String NAME = "name";
    public static final String LOCATION = "location";
    public static final String KEY = "key";
    public static final String PAGE_TOKEN_NEXT = "pagetoken";
    public static final String SENSOR = "sensor";
    public static final String RADIUS = "radius";
    public static final String PHOTOREFENCE = "photoreference";
    public static final String MAX_WIDTH = "maxwidth";
    public static final String MAX_HEIGHT = "maxheight";
    public static final String PLACEID = "placeid";

    public static final String BASE_URL = "https://maps.googleapis.com/maps/api/place/";
    public static final String GET_PLACE = "search/json";
    public static final String GET_PLACE_DETAIL = "details/json";
    public static final String GET_PHOTO_PLACE = BASE_URL + "photo";
}
