package com.api.placeshow.common;

import android.content.SharedPreferences.Editor;

public class PrefUtils {

    public static void putBoolean(String key, boolean defValue) {
        Editor editor = Constant.sPreferences.edit();
        editor.putBoolean(key, defValue);
        editor.commit();
    }

    public static boolean getBoolean(String key, boolean defValue) {
        return Constant.sPreferences.getBoolean(key, defValue);
    }

    public static void clearData() {
        Editor editor = Constant.sPreferences.edit();
        editor.clear();
        editor.commit();
    }
}
