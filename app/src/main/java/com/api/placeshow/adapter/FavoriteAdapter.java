package com.api.placeshow.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.api.placeshow.R;
import com.api.placeshow.activity.PlaceDetailActivity;
import com.api.placeshow.common.Constant;
import com.api.placeshow.common.Util;
import com.api.placeshow.model.Place;
import com.bumptech.glide.Glide;

import java.util.List;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteViewHolders> {

    private List<Place> listFavorite;
    private Context mContext;

    // Constructor of adapter
    public FavoriteAdapter(Context mContext, List<Place> listFavorite) {
        this.mContext = mContext;
        this.listFavorite = listFavorite;
    }

    @Override
    public FavoriteViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite, parent, false);
        // Listener click item favorite
        FavoriteViewHolders rcv = new FavoriteViewHolders(layoutView, new FavoriteViewHolders.ViewHolderClicks() {
            @Override
            public void clickFavorite(int position) {
                Intent intent = new Intent(mContext, PlaceDetailActivity.class);
                intent.putExtra(Constant.PLACE_ID, listFavorite.get(position).getIdPlace());
                intent.putExtra(Constant.NAME, listFavorite.get(position).getNamePlace());
                mContext.startActivity(intent);
            }
        });
        return rcv;
    }

    @Override
    public void onBindViewHolder(FavoriteViewHolders holder, int position) {
        // Show data on list favorite
        Place place = listFavorite.get(position);
        holder.tvName.setText(listFavorite.get(position).getNamePlace());
        holder.tvAddress.setText(listFavorite.get(position).getAddressPlace());
        // set image of place with api
        if(place.getPhotoPlace() != null) {
            String url = Util.getUrlPhoto(place.getPhotoPlace().get(0).getReferencePhoto());
            Glide.with(mContext)
                    .load(url)
                    .into(holder.imvPhoto);
        }else{
            // set image of place default
            holder.imvPhoto.setImageResource(R.drawable.default_photo);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return listFavorite.size();
    }

    public void setListFavorite(List<Place> favorites) {
        this.listFavorite = favorites;
        notifyDataSetChanged();
    }
}
