package com.api.placeshow.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.api.placeshow.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
    @BindView(R.id.imv_photo) protected ImageView imvPhoto;
    @BindView(R.id.tv_name) protected TextView tvName;
    @BindView(R.id.tv_address) protected TextView tvAddress;

    public ViewHolderClicks holderClicks;

    public FavoriteViewHolders(View convertView, ViewHolderClicks holderClicks) {
        super(convertView);
        // init component view favorite
        ButterKnife.bind(this, convertView);
        this.holderClicks = holderClicks;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(holderClicks != null){
            // listener lick each item favorite
            holderClicks.clickFavorite(getLayoutPosition());
        }
    }

    public interface ViewHolderClicks {
        void clickFavorite(int position);
    }
}
