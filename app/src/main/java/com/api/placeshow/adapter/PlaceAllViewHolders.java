package com.api.placeshow.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.api.placeshow.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class PlaceAllViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {
    @BindView(R.id.imv_place_all) protected ImageView imvPlaceAll;
    @BindView(R.id.tv_name_place_all) protected TextView tvNameAll;
    @BindView(R.id.tv_address_place_all) protected TextView tvAddressAll;

    public ViewHolderClicks holderClicks;

    public PlaceAllViewHolders(View convertView, ViewHolderClicks holderClicks) {
        super(convertView);
        // init component view place
        ButterKnife.bind(this, convertView);
        this.holderClicks = holderClicks;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (holderClicks != null) {
            // listener lick each item place
            holderClicks.clickPlace(getLayoutPosition());
        }
    }

    public interface ViewHolderClicks {
        void clickPlace(int position);
    }
}
