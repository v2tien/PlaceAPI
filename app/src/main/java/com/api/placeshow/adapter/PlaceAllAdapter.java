package com.api.placeshow.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.api.placeshow.R;
import com.api.placeshow.activity.PlaceDetailActivity;
import com.api.placeshow.common.Constant;
import com.api.placeshow.common.Util;
import com.api.placeshow.model.Place;
import com.bumptech.glide.Glide;

import java.util.List;


public class PlaceAllAdapter extends RecyclerView.Adapter<PlaceAllViewHolders> {

    private List<Place> lstPlace;
    private Context mContext;

    public PlaceAllAdapter(Context mContext, List<Place> lstPlace) {
        this.mContext = mContext;
        this.lstPlace = lstPlace;
    }

    @Override
    public PlaceAllViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place_new, parent, false);
        PlaceAllViewHolders rcv = new PlaceAllViewHolders(layoutView, new PlaceAllViewHolders.ViewHolderClicks() {
            @Override
            public void clickPlace(int position) {
                Intent intent = new Intent(mContext, PlaceDetailActivity.class);
                intent.putExtra(Constant.PLACE_ID, lstPlace.get(position).getIdPlace());
                intent.putExtra(Constant.NAME, lstPlace.get(position).getNamePlace());
                mContext.startActivity(intent);
            }
        });
        return rcv;
    }

    @Override
    public void onBindViewHolder(PlaceAllViewHolders holder, int position) {
        Place place = lstPlace.get(position);
        // Set data for each item place
        if(place.getPhotoPlace() != null) {
            String url = Util.getUrlPhoto(place.getPhotoPlace().get(0).getReferencePhoto());
            Glide.with(mContext)
                    .load(url)
                    .into(holder.imvPlaceAll);
        }
        holder.tvNameAll.setText(place.getNamePlace());
        holder.tvAddressAll.setText(place.getAddressPlace());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return lstPlace.size();
    }

    public void setListPlace(List<Place> places){
        this.lstPlace = places;
        notifyDataSetChanged();
    }
}
