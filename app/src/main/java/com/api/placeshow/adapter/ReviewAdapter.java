package com.api.placeshow.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RatingBar;
import android.widget.TextView;

import com.api.placeshow.R;
import com.api.placeshow.model.Review;

import java.util.List;

public class ReviewAdapter extends BaseAdapter {

    private Context mContext;
    private List<Review> lstReview;

    public ReviewAdapter(Context mContext, List<Review> lstReview) {
        this.mContext = mContext;
        this.lstReview = lstReview;
    }

    @Override
    public int getCount() {
        return lstReview.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setListReview(List<Review> reviews) {
        this.lstReview = reviews;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final HolderView holder;
        if (convertView == null) {
            // component of review
            holder = new HolderView();
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_review, parent, false);
            holder.tvName = (TextView)convertView.findViewById(R.id.tv_name_review);
            holder.tvTime = (TextView)convertView.findViewById(R.id.tv_time_review);
            holder.tvContent = (TextView)convertView.findViewById(R.id.tv_content_review);
            holder.ratingBar = (RatingBar)convertView.findViewById(R.id.rating_bar_review);
            convertView.setTag(holder);
        } else {
            holder = (HolderView) convertView.getTag();
        }
        // Set data review of place
        Review review = lstReview.get(position);
        holder.tvName.setText(review.getNameReview());
        holder.tvContent.setText(review.getContentReview());
        holder.tvTime.setText(review.getLastTimeReview());
        Double num = review.getRatingOfReview() * 10;
        int rating = Integer.valueOf(num.intValue());
        holder.ratingBar.setProgress(rating);

        return convertView;
    }

    public class HolderView {
        TextView tvName, tvTime, tvContent;
        RatingBar ratingBar;
    }
}
