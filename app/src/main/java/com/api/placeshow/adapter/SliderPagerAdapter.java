package com.api.placeshow.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.api.placeshow.R;
import com.api.placeshow.common.Util;
import com.api.placeshow.model.Photo;
import com.bumptech.glide.Glide;

import java.util.List;

public class SliderPagerAdapter extends PagerAdapter
{
    private List<Photo> lstImage;
    private LayoutInflater mInflate = null;
    private Context mContext = null;

    public SliderPagerAdapter(Context context, List<Photo> lstImage) {
        this.mContext = context;
        this.lstImage = lstImage;
        mInflate = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public Object instantiateItem(ViewGroup collection, int position)
    {
        ViewGroup layout;
        layout = (ViewGroup) mInflate.inflate(R.layout.item_slider, collection, false);
        ImageView imvPhoto = (ImageView)layout.findViewById(R.id.imv_place);

        Photo photo = lstImage.get(position);
        String url = Util.getUrlPhoto(photo.getReferencePhoto());
        Glide.with(mContext)
                .load(url)
                .into(imvPhoto);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return lstImage.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }
}
